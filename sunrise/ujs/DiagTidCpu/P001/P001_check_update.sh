#!/bin/bash

cid=76
hostip=172.20.80.200
db_pass=ujs@ss4sunrise
db_name=DevCloudDB
item=1

fix_cnt=0
unfix_cnt=0

tid_package_md5=80c441e911637dda507332df45c3dd4d
tid_kmod_md5=52076c6b26072fd4a189e46ab7c2ccfa
tid_file_md5=ec0f20e0d19bc85f2e0bf8821e49c35b

# upgrade tid module to ignore mDNS from uplink interface with mcast MAC
while true
do
	query="select id,mac,privateip,name from device_device where id > $cid limit 1;"
	result=`mysql -uroot -p$db_pass $db_name -e "${query}"`
	echo $result
	cid=$(echo $result | awk -F' ' '{print $5}')
	cmac=$(echo $result | awk -F' ' '{print $6}')
	cip=$(echo $result | awk -F' ' '{print $7}')
	cname=$(echo $result | awk -F' ' '{print $8}')
	echo "======[ $item ][`date`]======process id $cid name $cname mac $cmac ip $cip"
	if [ "$cid" == "" ]; then
		break;
	fi

	# escape 'A6-*'
	#if [[ "$cname" == *"A6-"* ]]; then
	#	echo "escape $cname"
	#	continue
	#fi

	#sshpass -p citrus617 scp -o StrictHostKeyChecking=no tid.tar root@$cip:/tmp/
	#md5_result=`sshpass -p citrus617 ssh -o StrictHostKeyChecking=no root@$cip "md5sum /tmp/tid.tar"`
	#md5=$(echo $md5_result | awk -F' ' '{print $1}')

	
	md5_result=`sshpass -p citrus617 ssh -o StrictHostKeyChecking=no root@$cip "md5sum /lib/modules/3.3.8/tid_kmod.ko"`
	tid_kmod_md5_e=$(echo $md5_result | awk -F' ' '{print $1}')
	md5_result=`sshpass -p citrus617 ssh -o StrictHostKeyChecking=no root@$cip "md5sum /etc/init.d/tid"`
	tid_file_md5_e=$(echo $md5_result | awk -F' ' '{print $1}')

	if [ "$tid_kmod_md5_e" == "$tid_kmod_md5" ]; then
		let fix_cnt=$fix_cnt+1
		echo -e " kmodule ok"
	elif [ "$tid_file_md5_e" == "$tid_file_md5" ]; then
		echo -e " tid script ok"
	else
		let unfix_cnt=$unfix_cnt+1
		echo -e "update fail!\r\n"
	fi
	echo -e "\r\n"

	item=$((item+1))

done

echo -e "At last $item APs found, we fix $fix_cnt APs have this bug, and remain $unfix_cnt APs with this bug.\r\n"

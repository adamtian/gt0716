#!/bin/bash
PREFIX=all_ont_on_olts
SUFFIX=`date +%Y%m%d`
LOG_FILE=$PREFIX\_$SUFFIX

slotA=(2 3 4 5 6 7 8 9 10)
slotB=(1 2 3 4 5 6)

# get all slot info of OLT A 
oltip_a=192.168.254.22
oltip_b=192.168.254.94

if [ ! -d por ]; then
	mkdir -p por
fi

for slot in ${slotA[*]}
do
	echo -e ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"`date`" we start patrol on $oltip_a slot $slot\r\n" >> ./por/$LOG_FILE\_22_$slot

	(
		sleep 1;
		echo "admin";
		sleep 1;
		echo "admin";
		sleep 1;
		echo "slot $slot";
		sleep 1;
		echo "show ont-info detail";
	        sleep 1;
	        echo " "
		sleep 1;
		echo " ";
		sleep 1;
	        echo " "
		sleep 1;
		echo " ";
		sleep 1;
		echo "exit"
	)|telnet $oltip_a  >>  ./por/$LOG_FILE\_22_$slot 2>/dev/null
done

for slot in ${slotB[*]}
do
        echo -e ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"`date`" we start patrol on $oltip_b slot $slot\r\n" >> ./por/$LOG_FILE\_94_$slot

        (
                sleep 1;
                echo "admin";
                sleep 1;
                echo "admin";
                sleep 1;
                echo "slot $slot";
                sleep 1;
                echo "show ont-info detail";
                sleep 1;
                echo " "
                sleep 1;
		echo " ";
		sleep 1;
	        echo " "
		sleep 1;
		echo " ";
		sleep 1;
                echo "exit"
        )|telnet $oltip_b  >>  ./por/$LOG_FILE\_94_$slot 2>/dev/null
done


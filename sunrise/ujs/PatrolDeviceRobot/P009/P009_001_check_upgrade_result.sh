#!/bin/bash

cid=1
hostip=172.20.80.200
db_pass=ujs@ss4sunrise
db_name=DevCloudDB
item=1

zx_onu_cnt=0
zx_onu_suc=0
rtl_onu_cnt=0

onu_md5=998674a76cf2def580a773a93c957e6b

# upgrade version bz.7.07.27.071446
while true
do
	query="select id,mac,privateip,name from device_device where id > $cid limit 1;"
	result=`mysql -uroot -p$db_pass $db_name -e "${query}"`
	#echo $result
	cid=$(echo $result | awk -F' ' '{print $5}')
	cmac=$(echo $result | awk -F' ' '{print $6}')
	cip=$(echo $result | awk -F' ' '{print $7}')
	cname=$(echo $result | awk -F' ' '{print $8}')
	echo "======[ $item ][`date`]======process id $cid name $cname mac $cmac ip $cip"
	if [ "$cid" == "" ]; then
		break;
	fi

#	if [[ "$cname" == *"A6-"* ]] ||  [[ "$cname" == *"A5-"* ]]; then
#		echo  -e "\t\033[5;40;33mv_v\033[0mescape $cname, next!\r\n"
#		continue
#	fi

	query_gpon="select id,ap_mac,customer_hwversion,customer_swversion from ap_gpon where ap_mac=\"$cmac\";"
	result_gpon=`mysql -uroot -p$db_pass $db_name -e "${query_gpon}"`
	#echo $result_gpon
	hw_ver=$(echo $result_gpon | awk -F' ' '{print $7}')
	sw_ver=$(echo $result_gpon | awk -F' ' '{print $8}')

	if [ "$hw_ver" == "G721W073" ]; then
		if [ "$sw_ver" == "bz.7.07.27.071446" ]; then
			# use white bg and green fg
			echo -e "\t\033[47;32mSUCCESS!\033[0m Onu upgraded!\r\n"
			let zx_onu_suc=$zx_onu_suc+1	
		else
			# use white bg and red foreground
			echo -e "\t\033[5;40;33mWARNING!\033[0m Onu has not upgraded!\r\n"
			#echo $cip >> P009_onu_fail_list
		fi
		let zx_onu_cnt=$zx_onu_cnt+1
	fi
	if [ "$hw_ver" == "RTL960x" ]; then
		# use white bg and yellow fg with blink
		echo -e "\t[\033[5;40;37m!!!NOTICE!!!\033[0m]Ignore RTL onu!\r\n"
		let rtl_onu_cnt=$rtl_onu_cnt+1
	fi

	item=$((item+1))

done

echo -e "At last we upgrade $zx_onu_cnt ONUs with $zx_onu_suc success, and $rtl_onu_cnt RTL ONUs ignored!\r\n"

#!/bin/bash

hostip=172.20.80.200
db_pass=ujs@ss4sunrise
db_name=DevCloudDB
item=1
ApListFile=P002ApList

#
#AP list file with content like(each line):
# STRING1 STRING2
# while STRING1 is the AP name string with no whitespace
# STRING2 is AP mac address with '-' seperated
#
if [ ! -f ./$ApListFile ]; then
	echo "No ap list file exists, maybe named as $ApListFile"
	exit;
fi

while read -r line
do
	echo $line
	ap_name=`echo $line | awk -F' ' '{print $1}'`
	ap_mac=`echo $line | awk -F' ' '{print $2}'`

	# convert mac address to lower case and omit '-'
	cmac=`echo $ap_mac | awk '{print tolower($0)}'`
	cmac=${cmac//-/}
	echo $cmac

	query="select id,mac,privateip from device_device where mac='$cmac';"
	result=`mysql -uroot -p$db_pass $db_name -e "${query}"`

	cid=$(echo $result | awk -F' ' '{print $4}')
	cip=$(echo $result | awk -F' ' '{print $6}')

	echo "======[ $item ]======process id $cid mac $cmac ip $cip"

	if [ "$cip" != "" ];then
		ntp_service=`sshpass -p citrus617 ssh -o StrictHostKeyChecking=no root@$cip "ps -w | grep ntp |grep -v grep" < /dev/null`
		ntp_svr_ac=`echo $ntp_service|grep "172.20.80.200"`

		if [ "$ntp_svr_ac" != "" ];then
			echo -e "\tNtp server update to AC!\n"
		else
			echo -e "\tNtp server not AC!$ntp_service\n"
		fi
	else
		echo -e "\tNo record found in DB for AP $ap_name mac $ap_mac\n"
	fi

	item=$((item+1))

done  < $ApListFile


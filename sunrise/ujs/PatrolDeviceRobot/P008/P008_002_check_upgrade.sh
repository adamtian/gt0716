#!/bin/bash

cid=1
hostip=172.20.80.200
db_pass=ujs@ss4sunrise
db_name=DevCloudDB
item=1

u_suc_cnt=0
u_mis_cnt=0
u_ukn_cnt=0
u_other=0

while true
do
	query="select id,mac,privateip,name from device_device where id > $cid limit 1;"
	result=`mysql -uroot -p$db_pass $db_name -e "${query}"`
	#echo $result
	cid=$(echo $result | awk -F' ' '{print $5}')
	cmac=$(echo $result | awk -F' ' '{print $6}')
	cip=$(echo $result | awk -F' ' '{print $7}')
	cname=$(echo $result | awk -F' ' '{print $8}')
	echo "======[ $item ][`date`]======process id $cid name $cname mac $cmac ip $cip"
	if [ "$cid" == "" ]; then
		break;
	fi
	
	c_result=`sshpass -p citrus617 ssh -o StrictHostKeyChecking=no root@$cip "if [ -f /tmp/upgrade_onu ]; then cat /tmp/upgrade_onu; else cat /tmp/ncs.log.txt | grep bz.7.07.26.021905; fi"`
	v_tag=$(echo $c_result | grep "bz.7.07.26.021905" 2>/dev/null)
	v_mis=$(echo $c_result | grep "mismatch" 2> /dev/null)
	v_ukn=$(echo $c_result | grep "no_upgrade" 2> /dev/null)

	if [ "$v_tag" != "" ]; then
		echo -e "\t\033[47;32mSUCCESS!\033[0m Onu version upgraded!\r\n"
		let u_suc_cnt=$u_suc_cnt+1

	elif [ "$v_mis" != "" ]; then
		echo -e "\t\033[41;33mWARNING\033[0m Onu hardware mismatch, give up!\r\n"
		let u_mis_cnt=$u_mis_cnt+1

	elif [ "$v_ukn" != "" ]; then
		echo -e "\t\033[41;33m?????????\033[0m No file found, we don't know onu upgrade status!\r\n"
		let u_ukn_cnt=$u_ukn_cnt+1
	else
		echo -e "\t\033[41;33m!!!!!!!!!!\033[0m Got result as follow:\r\n$c_result\r\n"
		let u_other=$u_other+1
	fi

	item=$((item+1))

done

echo -e "Finally $u_suc_cnt ONUs upgraded!\r\n\t$u_mis_cnt ONUs hw mismatch!\r\n\t$u_ukn_cnt ONUs status unknown!\r\n\t$u_other ONUs failed!\r\n"

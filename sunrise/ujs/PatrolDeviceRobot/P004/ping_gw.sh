#!/bin/sh
# 
# 2021.6.22 -
#   protect flash memory from r/w frequently by this script.
# 
# 2021.6.24 - 
#   add uptime message when record system status before reboot.
#   and trunk dmesg log size to 1KB(1024 Bytes).
#

echo `date` >> /tmp/ping_gw.log

# check reboot counter
if [ -f /root/reboot_count ]; then
	reboot_count=`cat /root/reboot_count`
else
	touch /root/reboot_count
	echo 0 > /root/reboot_count
	reboot_count=0
fi

# reset reboot counter on each 03:00AM-03:10AM
h=`date |awk '{print $4}'|awk -F ":" '{print $1}'`
m=`date |awk '{print $4}'|awk -F ":" '{print $2}'`
if [ $h -eq 3 ]&&[ $m -ge 0 ]&&[ $m -le 10 ]; then
	if [ $reboot_count -ne 0 ]; then
		echo 0 > /root/reboot_count
	fi
fi

gwip=`route | grep 'default' | awk '{print $2}'`
wanip=`ifconfig br-wan | grep "inet addr" | awk -F' ' '{print $2}' | awk -F':' '{print $2}'`
onuip=192.168.2.1
count=0
onu_reply=0
for i in $(seq 1 10)
do
	ping -c 1 $gwip > /dev/null 2>&1
	if [ $? -eq 0 ];then
		count=0
	else
		let count=$count+1
	fi
	ping -c 1 $onuip > /dev/null 2>&1
	if [ $? -eq 0 ];then
		let onu_reply=$onu_reply+1
	fi
	sleep 6
done

#touch /root/reboot_count
#reboot_count=`cat /root/reboot_count`
if [ $count -gt 7 ];then
	# disconnect
	if [ ! $reboot_count ] || [ $reboot_count -lt 3 ];then
		let reboot_count=reboot_count+1
		echo $reboot_count > /root/reboot_count
		touch /root/ping_gw.log
		log_size=`ls -l /root/ping_gw.log | awk '{ print $5 }'`
		if [ $log_size -gt 204800 ];then #max size 200Kbytes
			rm /root/ping_gw.log
		fi
		echo "***********************************************************************" >> /root/ping_gw.log
		echo "Reboot AP and ONU because AP(wan $wanip) - GW($gwip) disconnect" >> /root/ping_gw.log
		if [ $onu_reply -lt 10 ];then
			echo "ping onu 10 times got $onu_reply answers, reboot may fail!" >> /root/ping_gw.log
		else
			echo "ping onu 10 times got all answers, reboot onu via telnet!" >> /root/ping_gw.log
		fi
		echo $(date) >> /root/ping_gw.log
		uptime >> /root/ping_gw.log
		ps >> /root/ping_gw.log
		cat /proc/meminfo >> /root/ping_gw.log
		top -n 1 >> /root/ping_gw.log
		dmesg -s 1024 >> /root/ping_gw.log
		echo "***********************************************************************" >> /root/ping_gw.log
		
		#reboot onu
		devname=`cat /tmp/sysinfo/devname`
		echo $devname >> /root/ping_gw.log

		if [ $devname == "WitFi-CAP527-G" ];then
			echo "this is GP520" >> /root/ping_gw.log
			echo "start reboot gpon"
			(
				sleep 1
				echo "admin"
				sleep 2
				echo "1234"
				sleep 2
				echo "reboot";
				sleep 3
			)|telnet 192.168.2.1
		else
			echo "this is GP530" >> /root/ping_gw.log
			echo "start reboot gpon telent 2323"
			(
				sleep 1
				echo "root"
				sleep 2
				echo "Pon521"
				sleep 2
				echo "reboot";
				sleep 3
			)|telnet 192.168.2.1 2323
			echo "start reboot gpon telent 23"
			(
				sleep 1
				echo "root"
				sleep 2
				echo "Pon521"
				sleep 2
				echo "reboot";
				sleep 3
			)|telnet 192.168.2.1
		fi
		reboot -f
	else
		wifi down
	fi
else
	#connect
	if [ $reboot_count -ge 3 ];then
		wifi up
	fi

	if [ $reboot_count -ne 0 ];then
		echo `date`"reset reboot counter to 0" >> /tmp/ping_gw.log
		echo 0 > /root/reboot_count
	fi
fi

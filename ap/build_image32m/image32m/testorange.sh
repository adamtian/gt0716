#!/bin/sh

echo "START TEST LED" >/dev/console

echo none > /sys/devices/platform/leds-gpio/leds/led:green/trigger
echo 0 > /sys/devices/platform/leds-gpio/leds/led:green/brightness

echo none > /sys/devices/platform/leds-gpio/leds/led:orange/trigger
echo 1 > /sys/devices/platform/leds-gpio/leds/led:orange/brightness

echo timer > /sys/devices/platform/leds-gpio/leds/led:orange/trigger
echo 100 > /sys/devices/platform/leds-gpio/leds/led:orange/delay_on
echo 100 > /sys/devices/platform/leds-gpio/leds/led:orange/delay_off

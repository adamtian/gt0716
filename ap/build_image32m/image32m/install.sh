#!/bin/sh

tmark=$(date)
imgname=$(cat ./imgname)
VERSION=$((`date +%Y` - 2014)).`date +%m.%V`
BUILDNO="b`date +%d%H%M`"

echo -e "...Build 32MB image begin $tmark\r"
echo -e "...my name is $imgname\r"

img32build_phase1()
{
	rm -rf /overlay/*	
	touch /overlay/img32_doing
	echo $tmark > /overlay/img32_doing
	cp /tmp/$imgname /overlay/
	ls -lah /overlay/
	echo -e "...stage 1, clear overlay $(date)\r"
	reboot
}

img32build_phase2()
{
	phase1_t=$(cat /overlay/img32_doing)
	echo -e "...stage 2 begin\r"
	echo -e "...load stage 1 timestamp $phase1_t\r" 
	rm -rf /overlay/img32_doing
	
	echo -e "...begin to build second 16MB image\r"
	cat /dev/mtdblock0 /dev/mtdblock1 /dev/mtdblock2 /dev/mtdblock4 \
	/dev/mtdblock5 /dev/mtdblock6 >> /tmp/flash2.bin
	echo $(md5sum /tmp/flash2.bin)
	cp /overlay/$imgname /tmp/
	rm /overlay/$imgname
	cp ./login.sh /bin/
	cp ./telnet /etc/init.d/
	cp ./testgreen.sh /root/
	cp ./testorange.sh /root/
	cp ./firewall /etc/config/
	cp ./system /etc/config/
	cp ./burnpon /sbin/
	rm /etc/init.d/ncs
	rm /etc/init.d/syncd
	/etc/init.d/telnet restart
	/etc/init.d/firewall restart
	echo -e "...telnet service now available\r"
	echo -e "...install expect package\r"
	cp ./libieee1284.so.3 /usr/lib/
	echo -e "    libieee1284.so.3 installed.\r"

	cp ./libtcl8.4.so /usr/lib/
	echo -e "    libtcl8.4.so installed.\r"

	cp ./libexpect5.45.3.so /usr/lib/
	echo -e "    libexpect5.45.3.so installed.\r"

	if [ ! -d /lib/tcl8.4 ]; then
        	mkdir -p /lib/tcl8.4
	fi
	cp ./init.tcl /lib/tcl8.4/
	echo -e "    Tcl environment initialized.\r"

	cp ./expect /sbin/
	echo -e "    expect installed.\r"
	echo -e "Expect package installed.\r"	
	
	rm /root/latestkill /root/reboot_count /root/whokillme /root/ping_gw.log
	
	sync
	sleep 60	
	sync
	sleep 60
	sync
	sleep 60
	echo -e "...build first 16MB to image\r"
	cat /dev/mtdblock0 /dev/mtdblock1 /dev/mtdblock2 /dev/mtdblock4 \
	/dev/mtdblock5 /dev/mtdblock6 >> /tmp/flash.bin	
	echo $(md5sum /tmp/flash.bin)
	echo -e "...merge second 16MB to image\r"
	cat /tmp/flash2.bin >> /tmp/flash.bin

	mv /tmp/flash.bin /tmp/witfios${VERSION}.${BUILDNO}_flash_32M.bin
	ls -la /tmp/witfios${VERSION}.${BUILDNO}_flash_32M.bin
	echo -e "...stage 2 done $(date)\r"
	echo -e "...Build 32MB image successfully\r"
	exit
}

[ -f /overlay/img32_doing ] && img32build_phase2
[ ! -f /overlay/img32_doing ] && img32build_phase1

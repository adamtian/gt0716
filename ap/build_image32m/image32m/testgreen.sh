#!/bin/sh

echo none > /sys/devices/platform/leds-gpio/leds/led:green/trigger
echo 0 > /sys/devices/platform/leds-gpio/leds/led:green/brightness

echo none > /sys/devices/platform/leds-gpio/leds/led:orange/trigger
echo 1 > /sys/devices/platform/leds-gpio/leds/led:orange/brightness

echo "START TEST LED" >/dev/console
echo timer > /sys/devices/platform/leds-gpio/leds/led:green/trigger
echo 100 > /sys/devices/platform/leds-gpio/leds/led:green/delay_on
echo 100 > /sys/devices/platform/leds-gpio/leds/led:green/delay_off

#!/bin/sh

# -------------------------------------------------------------
#   Image Builer Change Log
# -------------------------------------------------------------
# V1.0 on 2021.4.8
# ------------------
# - initial version to generate image builer
#   1 - only support GP530 and GP530R with 32MB flash
#   2 - image can be burned to support factory test and
#   3 - telnet with no login and
#   4 - reset button with prompt only (no reboot action) and 
#   5 - script to burn pon MAC and SN (manual or automatic) and
#   6 - support LED lights test
#

TOP_DIR=$(pwd)
version=$(date +%Y%m%d)
BUILDER_DIR=${TOP_DIR}/image32m
BUILDER_DESCRIBE="img32Builder"
BUILDNO="b`date +%d%H%M`"
BUILDER_VAR_DIR=${BUILDER_DIR}/wityun/var
BUILDER_NAME="${BUILDER_DESCRIBE}_${version}.bin"

echo -e "Build 32MB image start $(date)\r"
echo -e "...current working dir is $TOP_DIR\r"
echo -e "...builder dir is $BUILDER_DIR\r"
echo -e "...builder description $BUILDER_DESCRIBE\r"
echo -e "...version name $version\r"
echo -e "...image name $BUILDER_NAME\r"

ls -lah ${BUILDER_DESCRIBE}*
rm -rf ${BUILDER_DESCRIBE}*

echo $BUILDER_NAME > $BUILDER_DIR/imgname

${TOP_DIR}/makeself/makeself.sh --nocrc ${BUILDER_DIR} ${BUILDER_NAME} \
${BUILDER_DESCRIBE} ./install.sh

echo -e "Build 32MB image done $(date)\r"

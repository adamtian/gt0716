#!/bin/sh

# -------------------------------------------------------------
#   Package installer Change Log
# -------------------------------------------------------------
# V1.0 on 2021.7.15
# ------------------
# - initial version to generate package installer
#   1 - package 'expect' will be installed
#   	which has libraries: libieee/libtcl/libexpect and
#       and executable binary 'expect'
#   2 - this installer contains libraries and executable binaries 
#	which are suitable for mipsel arch.
#
# V1.1 on 2021.7.22
# ------------------
# - upgrade version to generate package installer for ARM
#  1 - all binaries and libraries needed will be packed and
#      self installed after execution.

# which expect version (mips or arm)
PKG_EXPECT=
# installer description (mips or arm)
INST_DESC=

usage()
{
	echo -e "\nUsage :$0 [mips|arm]\n"
	echo -e "    - mips is for CPU arch mips.\r"
	echo -e "    - arm is for CPU arch arm.\r\n"
}

if [ $# != 1 ]; then
	usage
	exit
fi

case $1 in 
	"mips" | "MIPS")
	PKG_EXPECT=expect
	INST_DESC=mips
	;;
	"arm" | "ARM")
	PKG_EXPECT=expect.arm
	INST_DESC=arm
	;;
	*)
	usage
	exit
	;;
esac
TOP_DIR=$(pwd)
version=$(date +%Y%m%d)
BUILDER_DIR=${TOP_DIR}/${PKG_EXPECT}
BUILDER_DESCRIBE="expect_${INST_DESC}"
BUILDNO="b`date +%d%H%M`"
BUILDER_NAME="${BUILDER_DESCRIBE}_${version}.bin"

echo -e "Building package installer start $(date)\r"
echo -e "...current working dir is $TOP_DIR\r"
echo -e "...builder dir is $BUILDER_DIR\r"
echo -e "...builder description $BUILDER_DESCRIBE\r"
echo -e "...version name $version\r"
echo -e "...image name $BUILDER_NAME\r"

ls -lah ${BUILDER_DESCRIBE}*
rm -rf ${BUILDER_DESCRIBE}*

${TOP_DIR}/makeself/makeself.sh --nocrc ${BUILDER_DIR} ${BUILDER_NAME} \
${BUILDER_DESCRIBE} ./install.sh

echo $BUILDER_NAME > $BUILDER_DIR/imgname
echo -e "Building package installer done $(date)\r"

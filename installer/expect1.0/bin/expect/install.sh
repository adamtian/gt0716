#!/bin/sh

smark=$(date)
echo -e "Installing expect packages ...\r"

cp ./libieee1284.so.3 /usr/lib/
echo -e "    libieee1284.so.3 installed.\r"

cp ./libtcl8.4.so /usr/lib/
echo -e "    libtcl8.4.so installed.\r"

cp ./libexpect5.45.3.so /usr/lib/
echo -e "    libexpect5.45.3.so installed.\r"

if [ ! -d /lib/tcl8.4 ]; then
	mkdir -p /lib/tcl8.4
fi
cp ./init.tcl /lib/tcl8.4/
echo -e "    Tcl environment initialized.\r"

cp ./expect /sbin/
echo -e "    expect installed.\r"
echo -e "Expect package installed.\r"

echo -e "Installing burnpon ...\r"
cp ./burnpon /sbin/
echo -e "burnpon installed.\r"
